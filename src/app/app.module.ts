import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule}    from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { SearchBlockComponent } from './search-block/search-block.component';
import { FilmService } from './film.service';

@NgModule({
  imports: [
    NgSelectModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers:[FilmService],
  declarations: [
    AppComponent,
    DashboardComponent,
    SearchBlockComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
