export class Film {
    constructor(
        public description: string,
        public image_code: string,
        public year: number,
        public value: string,
        public obj_type: string,
        public link_code: string,
        public label: string,
        public id: number
    ) {
    }
}
