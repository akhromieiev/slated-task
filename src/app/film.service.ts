import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpClientJsonpModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Film } from './film.model';
import { createRequestOption } from './request-util';
import { map } from 'rxjs/operators';

export type EntityResponseType = HttpResponse<Film>;

@Injectable()
export class FilmService {

  private resourceUrl = 'http://www.slated.com/films';

  constructor(private http: HttpClient,
    private jsonpHttp: HttpClientJsonpModule) { }


  query(req?: any): Observable<Film[]> {
    const methodUrl = '/autocomplete/profiles/';
    const options = createRequestOption(req);
    const params = options.toString();
    const url = this.resourceUrl + methodUrl;

    return this.http.jsonp(`${url}?${params}`, 'callback')
      .pipe(map((res: Film[]) => { return res; }));
  }

  private convertResponse(res: EntityResponseType): EntityResponseType {
    const body: Film = this.convertItemFromServer(res.body);
    return res.clone({ body });
  }

  private convertArrayResponse(res: HttpResponse<Film[]>): HttpResponse<Film[]> {
    const jsonResponse: Film[] = res.body;
    const body: Film[] = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      body.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return res.clone({ body });
  }

  /**
   * Convert a returned JSON object to Film.
   */
  private convertItemFromServer(film: Film): Film {
    const copy: Film = Object.assign({}, film);
    return copy;
  }

  /**
   * Convert a Film to a JSON which can be sent to the server.
   */
  private convert(film: Film): Film {
    const copy: Film = Object.assign({}, film);
    return copy;
  }
}
