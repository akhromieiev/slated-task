import { Component, OnInit } from '@angular/core';
import { Film } from '../film.model';
import {Subject} from 'rxjs/Rx';
import { Observable } from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

2
import { HeroService } from '../hero.service';
import { FilmService } from '../film.service';

@Component({
  selector: 'search-block',
  templateUrl: './search-block.component.html',
  styleUrls: [ './search-block.component.css' ]
})
export class SearchBlockComponent implements OnInit {
  filmList$: any;
  filmListLoading = false;
  private filmListInput$ = new Subject<string>();

  term: string;
  selectedFilms: any;
  isLoading = false;

  constructor(private filmService: FilmService) {}

  ngOnInit(){
    // this.filmService.query({term: 'asas'}).subscribe((res)=>{
    //   console.log(res);
    // });
    this.filmList$ = this.filmListInput$
           .pipe(tap(() => this.isLoading = true))
           .debounceTime(500)
           .distinctUntilChanged()
           .switchMap((searchTerm) => this.filmService.query({
             term: searchTerm
           }).pipe(
               map((response) => response),
               catchError(() => of([])), // empty list on error
               tap(() => this.isLoading = false)
           ));
 }

}

